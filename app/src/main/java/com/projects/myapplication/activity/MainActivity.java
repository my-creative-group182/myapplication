package com.projects.myapplication.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.projects.myapplication.R;
import com.projects.myapplication.activity.MenuActivity;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private Button btnClick;
    protected Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: Started.");

        getLayoutObjects();
        setLayoutObjects();

    }


    private void getLayoutObjects() {
        btnClick = findViewById(R.id.btnClick);
        btnExit = findViewById(R.id.btnExit);
    }

    private void setLayoutObjects() {
        btnClick.setOnClickListener(btnOnClickListener);
        btnExit.setOnClickListener(btnOnClickListener);
    }

    private final View.OnClickListener btnOnClickListener = (View v) -> {

        final int id = v.getId();
        Intent myIntent;
        if (id == R.id.btnClick) {
            myIntent = new Intent(MainActivity.this, MenuActivity.class);
            startActivity(myIntent);
        } else if (id == R.id.btnExit) {
            myIntent = new Intent(getApplicationContext(), MainActivity.class);
            myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // myIntent.putExtra("EXIT", true);
            startActivity(myIntent);
            finish();
            System.exit(0);
        }
    };
}